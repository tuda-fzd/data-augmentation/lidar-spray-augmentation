# Lidar Spray Augmentation

This is a data augmentation model to augment clear weather lidar point clouds with detections from road spray.

The code will be published shortly...

## Credits

C. Linnhoff, D. Scheuble, M. Bijelic, L. Elster, P. Rosenberger, and H. Winner, *"Simulating Road Spray in Lidar Sensor Models,”* submitted to IEEE Sensors Journal, 2022

If you find our work useful in your research, please consider citing: 

```
@ARTICLE{linnhoff2022,
  author={Linnhoff, Clemens and Schuble, Dominik and Bijelic, Mario and Elster, Lukas and Rosenberger, Philipp and Winner, Hermann},
  journal={IEEE Sensors Journal}, 
  title={Simulating Road Spray in Lidar Sensor Models}, 
  year={2022}
}
```